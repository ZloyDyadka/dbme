package main

import (
	"context"
	"log"
	"net"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/ZloyDyadka/dbme/pkg/snapshooter"

	"github.com/pkg/errors"
	"gitlab.com/ZloyDyadka/dbme"
	"golang.org/x/sync/errgroup"
)

const (
	DefaultBindAddr         = ":1488"
	DefaultCleanInterval    = time.Second * 10
	DefaultSnapshotInterval = time.Second * 5
	DefaultSnapshotFilePath = "./dbme.db"
)

func main() {
	if err := Run(); err != nil {
		log.Fatal(err)
	}
}

func Run() error {
	storage := dbme.NewDatabase()
	api := dbme.NewTCPAPI(storage)
	snapshoter := snapshooter.NewSnapshooter(storage)

	ln, err := net.Listen("tcp", DefaultBindAddr)
	if err != nil {
		return errors.Wrap(err, "listen TCP network")
	}

	f, err := os.Open(DefaultSnapshotFilePath)
	if err != nil && !os.IsNotExist(err) {
		return errors.Wrap(err, "open snapshot file")
	}

	if os.IsNotExist(err) {
		if err := os.MkdirAll(filepath.Dir(DefaultSnapshotFilePath), os.ModePerm); err != nil {
			return errors.Wrap(err, "make directory for snapshot")
		}
	}

	if !os.IsNotExist(err) {
		if err := storage.Load(context.Background(), f); err != nil {
			return errors.Wrap(err, "load snapshot")
		}
	}

	app, ctx := errgroup.WithContext(context.Background())

	app.Go(func() error {
		<-ctx.Done()
		if err := ln.Close(); err != nil {
			//err logger
		}

		return nil
	})

	app.Go(func() error {
		if err := api.Serve(ln); err != nil {
			return errors.Wrap(err, "serve TCP API")
		}

		return nil
	})

	app.Go(func() error {
		if err := storage.Cleaner(ctx, DefaultCleanInterval); err != nil {
			return errors.Wrap(err, "run storage cleaner")
		}

		return nil
	})

	app.Go(func() error {
		if err := snapshoter.Run(ctx, DefaultSnapshotInterval, DefaultSnapshotFilePath); err != nil {
			return errors.Wrap(err, "run database snapshooter")
		}

		return nil
	})

	if err := app.Wait(); err != nil {
		return err
	}

	return nil
}
