package dbme

import (
	"bufio"
	"context"
	"encoding/gob"
	"io"
	"reflect"
	"sync"
	"time"
	"unsafe"

	"github.com/google/btree"
	"github.com/pkg/errors"
)

var (
	ErrNotFound = errors.New("record not found")
)

type Database struct {
	keys           map[string]*Record
	expiredIndexes *btree.BTree
	l              sync.RWMutex
}

func NewDatabase() *Database {
	return &Database{
		keys:           make(map[string]*Record),
		expiredIndexes: btree.New(64),
	}
}

type PutOptions struct {
	TTL time.Time
}

func (db *Database) Put(ctx context.Context, key string, value string, opts PutOptions) error {
	db.l.Lock()
	defer db.l.Unlock()

	record := &Record{
		Key:   key,
		Value: value,
		Opts: RecordOpts{
			TTL: opts.TTL,
		},
	}

	return db.insertRecord(record)
}

func (db *Database) Delete(ctx context.Context, key string) error {
	db.l.Lock()
	defer db.l.Unlock()

	return db.deleteRecord(ctx, key)
}

func (db *Database) deleteRecord(ctx context.Context, key string) error {
	record, ok := db.keys[key]
	if !ok {
		return ErrNotFound
	}

	delete(db.keys, key)
	db.expiredIndexes.Delete(record)

	return nil
}

func (db *Database) Snapshot(ctx context.Context, w io.Writer) error {
	db.l.RLock()
	defer db.l.RUnlock()

	return db.makeSnapshot(ctx, w)
}

func (db *Database) makeSnapshot(ctx context.Context, w io.Writer) error {
	bufWriter := bufio.NewWriterSize(w, 1024*1024*4)
	encoder := gob.NewEncoder(bufWriter)

	record := reflect.TypeOf(Record{})
	for _, r := range db.keys {
		if r.Expired() {
			continue
		}

		recordEncodeVal := reflect.NewAt(record, unsafe.Pointer(r))
		if err := encoder.EncodeValue(recordEncodeVal); err != nil {
			return errors.Wrap(err, "encode value")
		}
	}

	if err := bufWriter.Flush(); err != nil {
		return errors.Wrap(err, "final flush of data")
	}

	return nil
}

func (db *Database) Load(ctx context.Context, r io.Reader) error {
	db.l.Lock()
	defer db.l.Unlock()

	return db.loadSnapshot(ctx, r)
}

func (db *Database) loadSnapshot(ctx context.Context, r io.Reader) error {
	record := &Record{}
	reflectValue := reflect.ValueOf(record)

	bufReader := bufio.NewReader(r)
	decoder := gob.NewDecoder(bufReader)

	for {
		if err := decoder.DecodeValue(reflectValue); err != nil {
			if err == io.EOF {
				break
			}

			return errors.Wrap(err, "decode value")
		}

		if err := db.insertRecord(record.Copy()); err != nil {
			return errors.Wrap(err, "insert record into database")
		}
	}

	return nil
}

func (db *Database) insertRecord(r *Record) error {
	db.keys[r.Key] = r
	if !r.Opts.TTL.IsZero() {
		db.expiredIndexes.ReplaceOrInsert(r)
	}

	return nil
}

var (
	emptyRecord = ""
)

func (db *Database) Read(ctx context.Context, key string) (string, error) {
	db.l.RLock()
	defer db.l.RUnlock()

	r, ok := db.keys[key]
	if !ok {
		return emptyRecord, ErrNotFound
	}

	if r.Expired() {
		return emptyRecord, ErrNotFound
	}

	return r.Value, nil
}

func (db *Database) Cleaner(ctx context.Context, interval time.Duration) error {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
		}

		if err := db.clean(ctx); err != nil {
			return errors.Wrap(err, "clean database")
		}
	}
}

func (db *Database) clean(ctx context.Context) error {
	i := &Record{
		Opts: RecordOpts{
			TTL: time.Now(),
		},
	}

	var expired []*Record
	db.expiredIndexes.AscendLessThan(i, func(i btree.Item) bool {
		record := i.(*Record)

		expired = append(expired, record)

		return true
	})

	db.l.Lock()
	defer db.l.Unlock()

	for _, r := range expired {
		if err := db.deleteRecord(context.Background(), r.Key); err != nil {
			causeErr := errors.Cause(err)
			switch causeErr {
			case ErrNotFound:
				continue
			default:
				return errors.Wrap(err, "delete record")
			}
		}
	}

	return nil
}

type Record struct {
	Key   string
	Value string
	Opts  RecordOpts
}

func (r *Record) Expired() bool {
	if r.Opts.TTL.IsZero() {
		return false
	}

	return r.Opts.TTL.Sub(time.Now()) <= 0
}

func (r *Record) Copy() *Record {
	copy := *r
	return &copy
}

func (r *Record) Less(item btree.Item) bool {
	r2 := item.(*Record)

	return r2.Opts.TTL.After(r.Opts.TTL)
}

type RecordOpts struct {
	TTL time.Time
}
