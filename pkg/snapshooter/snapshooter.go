package snapshooter

import (
	"context"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"os"
	"time"
)

type Snapshooter interface {
	Snapshot(ctx context.Context, w io.Writer) error
}

type DatabaseSnapshooter struct {
	s Snapshooter
}

func NewSnapshooter(s Snapshooter) *DatabaseSnapshooter {
	return &DatabaseSnapshooter{
		s: s,
	}
}

func (s *DatabaseSnapshooter) Run(ctx context.Context, interval time.Duration, outputPath string) error {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
		}

		if err := s.snapshot(ctx, outputPath); err != nil {
			return errors.Wrap(err, "make snapshot")
		}
	}
}

func (s *DatabaseSnapshooter) snapshot(ctx context.Context, outputPath string) error {
	f, err := ioutil.TempFile("/tmp", "snapshot")
	if err != nil {
		return errors.Wrap(err, "create temp file for snapshot")
	}
	defer f.Close()

	if err := s.s.Snapshot(ctx, f); err != nil {
		return errors.Wrap(err, "make snapshot")
	}

	if err := s.handleSnapshotFile(f, outputPath); err != nil {
		return errors.Wrap(err, "handle snapshot file")
	}

	return nil
}

func (s *DatabaseSnapshooter) handleSnapshotFile(f *os.File, outputPath string) error {
	if err := f.Sync(); err != nil {
		return errors.Wrap(err, "sync snapshot file")
	}

	_, err := os.Stat(outputPath)
	if err != nil && !os.IsNotExist(err) {
		return errors.Wrap(err, "check old snapshot for exists")
	}

	var removeFile string
	if !os.IsNotExist(err) {
		newName := outputPath + "_2"
		if err := os.Rename(outputPath, newName); err != nil {
			return errors.Wrap(err, "rename old snapshot")
		}

		removeFile = newName
	}

	if err := os.Rename(f.Name(), outputPath); err != nil {
		return errors.Wrap(err, "rename new snapshot")
	}

	if removeFile == "" {
		return nil
	}

	if err := os.Remove(removeFile); err != nil {
		return errors.Wrap(err, "remove old snapshot")
	}

	return nil
}
