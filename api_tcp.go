package dbme

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var (
	ErrEmptyCommand = errors.New("empty command")
)

type Storer interface {
	Put(ctx context.Context, key string, value string, opts PutOptions) error
	Read(ctx context.Context, key string) (string, error)
	Delete(ctx context.Context, key string) error
}

type TCPAPI struct {
	storage Storer
}

func NewTCPAPI(s Storer) *TCPAPI {
	return &TCPAPI{
		storage: s,
	}
}

func (a *TCPAPI) Serve(ln net.Listener) error {
	for {
		conn, err := ln.Accept()
		if err != nil {
			return errors.Wrap(err, "accept connection")
		}

		go func() {
			defer conn.Close()

			if err := a.processConnection(context.Background(), conn); err != nil {
				causeErr := errors.Cause(err)
				switch causeErr {
				case io.EOF:
					return
				default:
					log.Println(err)
				}
			}
		}()
	}
}

func (a *TCPAPI) processConnection(ctx context.Context, c net.Conn) error {
	cmdParser := NewCommandParser(bufio.NewReader(c))

	for {
		cmd, err := cmdParser.NextCommand(ctx)
		if err != nil {
			causeErr := errors.Cause(err)
			switch causeErr {
			case ErrEmptyCommand:
				continue
			default:
				return errors.Wrap(err, "get command")
			}
		}

		switch cmd.Type {
		case CommandTypeRead:
			if err := a.ExecuteReadCommand(ctx, c, cmd); err != nil {
				return errors.Wrap(err, "execute read command")
			}
		case CommandTypePut:
			if err := a.ExecutePutCommand(ctx, c, cmd); err != nil {
				return errors.Wrap(err, "execute read command")
			}

		case CommandTypeDel:
			if err := a.ExecuteDelCommand(ctx, c, cmd); err != nil {
				return errors.Wrap(err, "execute read command")
			}
		default:
			_, err = fmt.Fprintf(c, "%s\r\n", "unknown command")
			if err != nil {
				return errors.Wrap(err, "write response to client")
			}

		}
	}
}

func (a *TCPAPI) ExecutePutCommand(ctx context.Context, responseWriter io.WriteCloser, cmd *Command) error {
	putCMD, err := NewCommandPut(cmd)
	if err != nil {
		return errors.Wrap(err, "new put command")
	}

	opts := PutOptions{
		TTL: time.Unix(0, putCMD.TTL),
	}

	response := "ok"
	if err := a.storage.Put(ctx, putCMD.Key, putCMD.Value, opts); err != nil {
		response = err.Error()
	}

	_, err = fmt.Fprintf(responseWriter, "%s\r\n", response)
	if err != nil {
		return errors.Wrap(err, "write response to client")
	}

	return nil
}

func (a *TCPAPI) ExecuteDelCommand(ctx context.Context, responseWriter io.WriteCloser, cmd *Command) error {
	putCMD, err := NewCommandDel(cmd)
	if err != nil {
		return errors.Wrap(err, "new del command")
	}

	response := "ok"
	if err := a.storage.Delete(ctx, putCMD.Key); err != nil {
		response = err.Error()
	}

	_, err = fmt.Fprintf(responseWriter, "%s\r\n", response)
	if err != nil {
		return errors.Wrap(err, "write response to client")
	}

	return nil
}

func (a *TCPAPI) ExecuteReadCommand(ctx context.Context, responseWriter io.WriteCloser, cmd *Command) error {
	readCMD, err := NewCommandRead(cmd)
	if err != nil {
		return errors.Wrap(err, "new read command")
	}

	response, err := a.storage.Read(ctx, readCMD.Key)
	if err != nil {
		causeErr := errors.Cause(err)
		switch causeErr {
		case ErrNotFound:
			break
		default:
			return errors.Wrap(err, "read from storage")
		}
	}

	_, err = fmt.Fprintf(responseWriter, "%d\r\n%s\r\n", len(response), response)
	if err != nil {
		return errors.Wrap(err, "write response to client")
	}

	return nil
}

type CommandType string

const (
	CommandTypeRead = "read"
	CommandTypePut  = "put"
	CommandTypeDel  = "del"
)

type Command struct {
	Type CommandType
	Args []string
}

type CommandPut struct {
	Key   string
	TTL   int64
	Value string
}

func NewCommandPut(cmd *Command) (*CommandPut, error) {
	cp := &CommandPut{}

	key := cmd.Args[0]
	ttl, err := strconv.ParseInt(cmd.Args[1], 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "parse ttl")
	}

	if ttl > 0 {
		cp.TTL = ttl
	}

	cp.Value = string(cmd.Args[3])
	cp.Key = key

	return cp, nil
}

type CommandDel struct {
	Key string
}

func NewCommandDel(cmd *Command) (*CommandDel, error) {
	cd := &CommandDel{}
	key := cmd.Args[0]
	cd.Key = key

	return cd, nil
}

type CommandRead struct {
	Key string
}

func NewCommandRead(cmd *Command) (*CommandRead, error) {
	cr := &CommandRead{}
	key := cmd.Args[0]
	cr.Key = key

	return cr, nil
}

type CommandParser struct {
	r *bufio.Reader
}

func NewCommandParser(r *bufio.Reader) *CommandParser {
	return &CommandParser{
		r: r,
	}
}

func (sc *CommandParser) NextCommand(ctx context.Context) (*Command, error) {
	data, err := sc.r.ReadBytes('\n')
	if err != nil {
		if len(data) > 0 {
			return nil, io.ErrUnexpectedEOF
		}

		if err == io.EOF {
			return nil, io.EOF
		}

		return nil, errors.Wrap(err, "read command")
	}

	if len(data) < 2 {
		return nil, errors.New("invalid command")
	}

	rawCommand := string(data[:len(data)-2])
	fields := strings.Fields(rawCommand)
	if len(fields) < 1 {
		return nil, ErrEmptyCommand
	}

	cmdType := fields[0]
	fields = fields[1:]

	switch cmdType {
	case CommandTypePut:
		fields, err = parseRawPutCommand(sc.r, fields)
		if err != nil {
			return nil, errors.Wrap(err, "parse raw put command")
		}
	case CommandTypeRead:
		fields, err = parseRawReadCommand(fields)
		if err != nil {
			return nil, errors.Wrap(err, "parse raw read command")
		}
	case CommandTypeDel:
		fields, err = parseRawDelCommand(fields)
		if err != nil {
			return nil, errors.Wrap(err, "parse raw del command")
		}
	}

	cmd := &Command{
		Type: CommandType(cmdType),
		Args: fields,
	}

	return cmd, nil
}

func parseRawReadCommand(fields []string) ([]string, error) {
	if len(fields) != 1 {
		return nil, errors.New("invalid arguments count")
	}

	key := fields[0]

	fields = append(fields, key)

	return fields, nil
}

func parseRawDelCommand(fields []string) ([]string, error) {
	if len(fields) != 1 {
		return nil, errors.New("invalid arguments count")
	}

	key := fields[0]

	fields = append(fields, key)

	return fields, nil
}

func parseRawPutCommand(r *bufio.Reader, fields []string) ([]string, error) {
	if len(fields) != 3 {
		return nil, errors.New("invalid arguments count")
	}

	length, err := strconv.Atoi(fields[2])
	if err != nil {
		return nil, errors.New("cannot read length " + err.Error())
	}

	value := make([]byte, length)

	_, err = io.ReadFull(r, value)
	if err != nil {
		return nil, errors.Wrap(err, "read value")
	}

	eof, err := r.ReadByte()
	if err != nil {
		return nil, err
	}

	if eof != '\r' {
		return nil, errors.Errorf("expected \r, got %s", string(eof))
	}

	eof, err = r.ReadByte()
	if err != nil {
		return nil, err
	}

	if eof != '\n' {
		return nil, errors.Errorf("expected \n, got %s", string(eof))
	}

	fields = append(fields[:2], fields[2:]...)
	fields = append(fields, string(value))

	return fields, nil
}
